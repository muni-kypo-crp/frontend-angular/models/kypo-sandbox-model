# KYPO Sandbox Model

This library contains frontend model of [KYPO Sandbox service](https://gitlab.ics.muni.cz/kypo-crp/backend-python/kypo-sandbox-service).

## Prerequisites
To use the library you need to have installed:
* NPM with access [KYPO registry](https://projects.ics.muni.cz/projects/kbase/knowledgebase/articles/153)

## Usage
To use the model in your project follow these steps:
1. Run `npm install @muni-kypo-crp/sandbox-model`
2. Import classes like usual, for example `import { SandboxDefinition } from '@muni-kypo-crp/sandbox-model`
